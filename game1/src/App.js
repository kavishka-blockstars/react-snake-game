import { useEffect, useState } from "react";
import Food from "./compoents/food";
import Snake from "./compoents/snake";

const getRandomPosition = ()=>{
  let min = 1;
  let max = 98;
  let x = Math.floor((Math.random()* (max-min+1)+min)/2)*2;
  let y = Math.floor((Math.random()* (max-min+1)+min)/2)*2;
  return[x,y];
}



function App() {
const[snake, setSnake] =useState([[0,0],[2,0]]);
const[food, setSFood] =useState(getRandomPosition());
const[direction, setDirection] =useState('RIGHT');

useEffect(()=>{
  document.onkeydown = onKeyDown;
  checkIfOutOfBorers();
  checkIfCollaps();
  checkIfEat();
  const interval = setInterval(() => {
    moveSnake()
   }, 200);
   return () => clearInterval(interval);

   
});


const onKeyDown = (e) =>{
  e = e || window.event;

  switch(e.keyCode){
    case 38:
      setDirection('UP');
      break;
    case 40:
      setDirection('DOWN');
      break;
    case 37: 
      setDirection('LEFT');
      break;
    case 39:
      setDirection('RIGHT');
      break;
  }
}

const moveSnake = ()=>{
  let dots = [...snake];
  let head = dots[dots.length-1];

  switch(direction){
    case "RIGHT":
      head = [head[0]+2, head[1]];
      break;
    case "LEFT":
      head = [head[0]-2, head[1]];
      break;
    case "UP":
      head = [head[0], head[1]-2];
      break;
    case "DOWN":
      head = [head[0], head[1]+2];
      break;
  }

  dots.push(head);
  dots.shift();

  setSnake(dots);
  console.log(dots);
}

function checkIfOutOfBorers(){
  let head = snake[snake.length-1];
  if (head[0] >=100 || head[1]>=100 || head[0]<0 || head[1]<0){
    gameOver();
  }
}

function gameOver(){
  alert(`Game over! Your score is ${snake.length-2}`);
  setSnake([[0,0],[2,0]])
  setDirection('RIGHT')
}

function checkIfCollaps(){
  let snakes = [...snake];
  let head = snakes[snakes.length-1];
  snakes.pop();
  snakes.forEach(dot=>{
    if(head[0]=== dot[0] && head[1]===dot[1]){
      gameOver();
    }
  })
}

function checkIfEat(){
  let head = snake[snake.length-1];
  let foods = food;
  if(head[0]===foods[0] && head[1]===foods[1]){
    setSFood(getRandomPosition);
    enlargeSnake();
  }
}

function enlargeSnake(){
  let newSnake = [...snake];
  newSnake.unshift([]);
  setSnake(newSnake);
}


  return (
    <div className="game-area">
        <Snake snake={snake} />
        <Food dot={food} />
    </div>
  );
}

export default App;
